#!/bin/sh
FILEWITHOUTEXT=EL1252


doOneFileUnglitchifyAndPlot()
{
  PVNAME=$1
	DEBUG=$2
  INPUTFILENAME=$FILEWITHOUTEXT.${PVNAME}.txt
  OUTPUTFILENAME=$FILEWITHOUTEXT.${PVNAME}.png
  rm -f $INPUTFILENAME $OUTPUTFILENAME
  if grep "${PVNAME}" $FILEWITHOUTEXT.txt >$INPUTFILENAME; then
    python unglitchify-and-plot.py $DEBUG $INPUTFILENAME $OUTPUTFILENAME | tee $INPUTFILENAME.glitches.txt
  fi
}

MYFILENAME=EL1252-21-02-18-16.00.57.txt
#if ! test -e $MYFILENAME; then
#	if ping -c 1 172.30.244.38; then
#		rsync -v -v torstenbogershausen@172.30.244.38:/home/torstenbogershausen/MCAG_setupMotionDemo.191003-base-7.0.3/epics/modules/ethercatmc//test/pythonscripts/logs/$MYFILENAME ./$FILEWITHOUTEXT.txt
#	fi
#else
#	echo >&2 "Can't get $MYFILENAME"
#	exit 1
#fi


#MYPVNAMES="UTCEL1252P0 PTPOffset PTPState"
#MYPVNAMES="PTPState"
#MYPVNAMES="DcToExtTimeOffsetEL6688 DcToExtTimeOffsetSystem WriteDoneExtTimeOffset"
MYPVNAMES="UTCEL1252P0 PTPOffset PTPState"
#DEBUG="--debug"
for MYPVNAME in $MYPVNAMES; do
  doOneFileUnglitchifyAndPlot $MYPVNAME $DEBUG|| {
		echo exitcode=$? ; exit 1
	}
done

#
# Cable disconnect and reconnect
#
MYPVNAMES="UTCEL1252P0 PTPState"
FILEWITHOUTEXT=EL1252-disconn
for MYPVNAME in $MYPVNAMES; do
  doOneFileUnglitchifyAndPlot $MYPVNAME $DEBUG|| {
		echo exitcode=$? ; exit 1
	}
done
