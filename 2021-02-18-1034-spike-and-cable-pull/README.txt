This directory consists of a (medium long) long-term evaluation
of the PTP-synchronization of a PTP signal feed into an EL6688 terminal.

The "goodness" of the synchronization was measured by recording
the raising edge of a 1Hz pulse, feed into a EL1252 terminal.
Look for UTCEL1252P0, the timestamp is converted from DC and TAI into UTC
inside the TwinCAT code.

There is one "incident" around 2021-02-17 22:30:00 and another between
2021-02:18 11:52 and 2021-02-18 12:58.

The first is one of the "spikes" we get from the non-optimized PTP server.

The second one is on purpose:
Pull the cable beetween the PTP server and the EL6688 and see what happens.
And put it back then.

See EL1252-disconn.PTPState.txt:
The PTPState drops from 9 (=slave) to 0 (=no operation).
Now the DC runs "free", without synchronization from PTP.

See EL1252-disconn.UTCEL1252P0.png for drift.

If you look carefully, you can see that the PTPState goes to 4 (=listening)
before jumping to 9 again.
No the synchronizations takes place, we see a little overshoot before being
back on track again.
