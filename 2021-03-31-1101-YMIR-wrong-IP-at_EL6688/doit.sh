#!/bin/sh
FILEWITHOUTEXT=EL6688

FALSE=0
export FALSE

#if ! test -s $FILEWITHOUTEXT.txt; then
  IPADDR=172.30.242.52
  if ping -c 1 $IPADDR; then
    rsync -v -v torstenbogershausen@$IPADDR:/home/torstenbogershausen/MCAG_setupMotionDemo/epics/modules/ethercatmc/test/pythonscripts/$FILEWITHOUTEXT.txt ./$FILEWITHOUTEXT.txt
  fi
#fi

#PTPState
#PVNAME=PTPState
#INPUTFILENAME=$FILEWITHOUTEXT.${PVNAME}.txt
#OUTPUTFILENAME=$FILEWITHOUTEXT.${PVNAME}.png
#if grep "${PVNAME}.*[0-9]" $FILEWITHOUTEXT.txt >$INPUTFILENAME; then
#  python unglitchify-and-plot.py $INPUTFILENAME $OUTPUTFILENAME
#fi

## 1 Hz Pulse, via PILS, new format
#PVNAME=UTCEL1252P0
#INPUTFILENAME=$FILEWITHOUTEXT.${PVNAME}.txt
#OUTPUTFILENAME=$FILEWITHOUTEXT.${PVNAME}.png
#rm -f $INPUTFILENAME $OUTPUTFILENAME
#if grep "${PVNAME}" $FILEWITHOUTEXT.txt >$INPUTFILENAME; then
#  python unglitchify-and-plot.py $INPUTFILENAME $OUTPUTFILENAME | tee $INPUTFI#LENAME.glitches.txt
#fi



#PTPOffset
PVNAME=PTPOffset
INPUTFILENAME=$FILEWITHOUTEXT.${PVNAME}.txt
OUTPUTFILENAME=$FILEWITHOUTEXT.${PVNAME}.png
if grep "${PVNAME}" $FILEWITHOUTEXT.txt >$INPUTFILENAME; then
  python unglitchify-and-plot.py $INPUTFILENAME $OUTPUTFILENAME
fi

#DcToExtTimeOffsetSystem
#PVNAME=DcToExtTimeOffsetSystem
#INPUTFILENAME=$FILEWITHOUTEXT.${PVNAME}.txt
#OUTPUTFILENAME=$FILEWITHOUTEXT.${PVNAME}.png
#if grep "${PVNAME}" $FILEWITHOUTEXT.txt >$INPUTFILENAME; then
#  python unglitchify-and-plot.py $INPUTFILENAME $OUTPUTFILENAME
#fi

if test $? -eq 0; then
  ls -l $FILEWITHOUTEXT*
fi
