#!/bin/sh
FILEWITHOUTEXT=EL1252


doOneFileUnglitchifyAndPlot()
{
  PVNAME=$1
	DEBUG=$2
  INPUTFILENAME=$FILEWITHOUTEXT.${PVNAME}.txt
  OUTPUTFILENAME=$FILEWITHOUTEXT.${PVNAME}.png
  rm -f $INPUTFILENAME $OUTPUTFILENAME
  if grep "${PVNAME}" $FILEWITHOUTEXT.txt >$INPUTFILENAME; then
    python unglitchify-and-plot.py $DEBUG $INPUTFILENAME $OUTPUTFILENAME | tee $INPUTFILENAME.glitches.txt
  fi
}

MYFILENAME=EL1252.txt
if ! test -e $MYFILENAME; then
	if ping -c 1 172.30.244.38; then
		rsync -v -v torstenbogershausen@172.30.244.38:/home/torstenbogershausen/MCAG_setupMotionDemo.191003-base-7.0.3/epics/modules/ethercatmc//test/pythonscripts/$MYFILENAME ./$FILEWITHOUTEXT.txt
	else
		echo >&2 "Can't get $MYFILENAME"
		exit 1
	fi
fi

MYPVNAMES="UTCEL1252P0 UTCCORREL1252P0 PTPOffset PTPState"
#DEBUG="--debug"
for MYPVNAME in $MYPVNAMES; do
  doOneFileUnglitchifyAndPlot $MYPVNAME $DEBUG|| {
		echo exitcode=$? ; exit 1
	}
done

