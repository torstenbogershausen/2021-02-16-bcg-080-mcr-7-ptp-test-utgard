import datetime
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.dates as mdates
import matplotlib.ticker as mticker
import re
import sys


"""
  Read a log file from the IOC, having lines like this:
  "LabS-MCAG:MC-MCU-07:DCtEL1252NSec 2020-05-26 13:32:16.926089 872013454  "
  "LabS-MCAG:MC-MCU-07:UTCEL1252P0 2021-02-12 11:23:52.868  2021-02-12 11:23:52.750938945 ..."
  The "UTCEL1252P0" PV gets recorded, when there is a 0 -> 1 transition
  on the 1Hz pulse, connected to the EL1252.
  The 1252 latches the time of the transition,
  and that is what we want to measure and plot.
  Note that there are sometimes 2 (or 3) transtions inside a second,
  but we feed only 1. Where the other ones come from, need to be
  investigated (noise on the line?)
  This is where the ung-litch function comes in:
  If we have more than one transition within the same seocnd,
  ignore both.
"""

debug = False


def main(argv=None):
    global debug
    if not argv:
        argv = sys.argv
    argc = len(argv)
    argvidx = 1
    if argv[argvidx] == "--debug":
        debug = True
        argvidx += 1
    if debug:
        print("len(argv)=%u " % argc)
        print("argv=%s" % argv)
    dontUseGlitchDetector = False
    output_file_name = None
    input_file_name = argv[argvidx]
    argvidx += 1
    if len(argv) > argvidx:
        output_file_name = argv[argvidx]
    argvidx += 1

    fid = open(input_file_name)

    lines = fid.readlines()

    # Collect date into an X- and Y- array
    x = []
    y = []
    oldSeconds = None
    quran_x_time = None
    quran_yvalue = None

    RE_MATCH_IOC_PV_NOT_FOUND = re.compile(
        r".*( \*\*\* Not connected \(PV not found\))"
    )
    RE_MATCH_IOC_PV_DISCONNECTED = re.compile(r".*( \*\*\* disconnected)")
    RE_MATCH_INVALID_UNDEFINED = re.compile(r".*(INVALID|<undefined>).*")
    # Regular expression to split the line into its components -
    # Do a basic sanity test
    #                                    name      date        time      raw
    RE_MATCH_IOC_LOG_LINE = re.compile(r"(\S+)\s+([0-9-]+)\s+([0-9:.]+)\s+(.+)")
    RE_MATCH_NON_DEGLITCH = re.compile(
        r".*:(PTPOffset|PTPState|DcToExtTimeOffsetEL6688|DcToExtTimeOffsetSystem|WriteDoneExtTimeOffset).*$"
    )
    # PTPState is special: We want to have a rectabgle-ish plot (and another scale)
    RE_MATCH_PTPSTATE = re.compile(r".*:(PTPState).*$")

    # 2021-02-12 11:23:52.750938945 sec-epoch-now=1613129033 sec_epoch_plc=1613129032
    #      year  month  day       hour     minute    second     nsec
    RE_MATCH_RAW_UTCEL1252P0 = re.compile(
        r"(\d+)-(\d+)-(\d+)\s+(\d+):(\d+):(\d+)\.(\d+)\D"
    )
    RE_MATCH_RAW_DIGITS_WITH_SIGN = re.compile(r"([0-9-]+)")
    RE_MATCH_DIGIT_IN_PARANTHES = re.compile(r"\(([0-9-]+)\)")

    old_ptpstate = None
    match_ptpstate = None
    for i in range(0, len(lines)):
        try:
            yvalue = None
            match_raw_DIGITS_WITH_SIGN = None
            line = lines[i]
            match_ioc_pv_not_found = RE_MATCH_IOC_PV_NOT_FOUND.match(line)
            if match_ioc_pv_not_found != None:
                continue
            match_ioc_pv_disconnected = RE_MATCH_IOC_PV_DISCONNECTED.match(line)
            if match_ioc_pv_disconnected != None:
                continue

            match_ioc_log_line = RE_MATCH_IOC_LOG_LINE.match(line)
            if match_ioc_log_line == None:
                print("re_match == None line=%s" % (line))
                sys.exit(1)

            pvname = match_ioc_log_line.group(1)
            date = match_ioc_log_line.group(2)
            time = match_ioc_log_line.group(3)
            raw = match_ioc_log_line.group(4)
            if debug:
                print(
                    "match_ioc_log_line=%s pvname=%s date=%s time=%s raw=%s "
                    % (match_ioc_log_line, pvname, date, time, raw)
                )
            match_invalid_undefined = RE_MATCH_INVALID_UNDEFINED.match(raw)
            if match_invalid_undefined != None:
                if debug:
                    print(
                        "match_invalid_undefined=%s raw=%s"
                        % (match_invalid_undefined, raw)
                    )
                continue
            match_raw_UTCEL1252P0 = RE_MATCH_RAW_UTCEL1252P0.match(raw)
            if debug:
                print("match_raw_UTCEL1252P0=%s" % (match_raw_UTCEL1252P0))
            if match_raw_UTCEL1252P0 != None:
                grp_idx = 1
                sdct_year = match_raw_UTCEL1252P0.group(grp_idx)
                grp_idx = grp_idx + 1
                sdct_month = match_raw_UTCEL1252P0.group(grp_idx)
                grp_idx = grp_idx + 1
                sdct_day = match_raw_UTCEL1252P0.group(grp_idx)
                grp_idx = grp_idx + 1
                sdct_hour = match_raw_UTCEL1252P0.group(grp_idx)
                grp_idx = grp_idx + 1
                sdct_minute = match_raw_UTCEL1252P0.group(grp_idx)
                grp_idx = grp_idx + 1
                sdct_sec = match_raw_UTCEL1252P0.group(grp_idx)
                grp_idx = grp_idx + 1
                sdct_nsec = match_raw_UTCEL1252P0.group(grp_idx)
                grp_idx = grp_idx + 1

                if debug:
                    print(
                        "match_raw_UTCEL1252P0=%s sdct_year=%s sdct_month=%s sdct_day=%s sdct_hour=%s sdct_minute=%s sdct_sec=%s sdct_nsec=%s"
                        % (
                            match_raw_UTCEL1252P0,
                            sdct_year,
                            sdct_month,
                            sdct_day,
                            sdct_hour,
                            sdct_minute,
                            sdct_sec,
                            sdct_nsec,
                        )
                    )
                nsec = sdct_nsec
                yvalue = float(nsec) / 1000.0
            else:
                match_raw_DIGITS_WITH_SIGN = RE_MATCH_RAW_DIGITS_WITH_SIGN.match(raw)
                if debug:
                    print(
                        "match_raw_DIGITS_WITH_SIGN=%s" % (match_raw_DIGITS_WITH_SIGN)
                    )

                if match_raw_DIGITS_WITH_SIGN != None:
                    grp_idx = 1
                    nsec = int(match_raw_DIGITS_WITH_SIGN.group(grp_idx))
                    if debug:
                        print("match_raw_DIGITS_WITH_SIGN=%d" % (nsec))
                    yvalue = float(nsec) / 1000.0
                else:
                    match_raw_DIGIT_IN_PARANTHES = RE_MATCH_DIGIT_IN_PARANTHES.match(
                        raw
                    )
                    if debug:
                        print(
                            "match_raw_DIGIT_IN_PARANTHES=%s"
                            % (match_raw_DIGIT_IN_PARANTHES)
                        )

                    if match_raw_DIGIT_IN_PARANTHES != None:
                        grp_idx = 1
                        nsec = int(match_raw_DIGIT_IN_PARANTHES.group(grp_idx))
                        if debug:
                            print("match_raw_DIGIT_IN_PARANTHES=%d" % (nsec))
                        yvalue = float(nsec)

            if yvalue is None:
                print("yvalue is None line=%s" % (line))
                print("  raw=%s" % (raw))
                # print("  len(nsecSplit)=%s nsec=%s" % (len(nsecSplit), nsec))
                # print("  nsec=%s type(nsec=%s)" % (nsec, type(nsec)))
                sys.exit(1)

            x_time = datetime.datetime.fromisoformat(date + " " + time)
            # Split off the fraction of a second.
            x_time_sec_res = str(x_time).split(".")[0]
            if debug:
                print("x_time_sec_res=%s oldSeconds=%s" % (x_time_sec_res, oldSeconds))
            match_non_deglitch = RE_MATCH_NON_DEGLITCH.match(line)
            match_ptpstate = RE_MATCH_PTPSTATE.match(line)
            if debug:
                print(
                    "match_ptpstate=%s old_ptpstate=%s" % (match_ptpstate, old_ptpstate)
                )
                print("match_non_deglitch=%s pvname=%s" % (match_non_deglitch, pvname))

            if dontUseGlitchDetector or (match_non_deglitch != None):
                # There is no need to suppress e.g. 2 PTPOffset measurements
                # done within the same second.
                if old_ptpstate != None:
                    x.append(x_time)
                    y.append(old_ptpstate)
                    old_ptpstate = None
                x.append(x_time)
                y.append(yvalue)
                if match_ptpstate != None:
                    old_ptpstate = yvalue
                continue

            if oldSeconds != None:
                if debug:
                    print(
                        "x_time=%s x_time_sec_res=%s oldSeconds=%s"
                        % (x_time, x_time_sec_res, oldSeconds)
                    )
                MAX_DELTA = 20000  # 20 usec
                if quran_yvalue is not None and abs(quran_yvalue - yvalue) > MAX_DELTA:
                    # The line has a '\n' at its end
                    print("glitch old_line %s      new_line  %s" % (quran_line, line))
                    # Ignore those valuse:
                    x_time = None
                    yvalue = None
                    quran_x_time = None
                    quran_yvalue = None
                    oldSeconds = None
                if (
                    x_time != None
                    and yvalue != None
                    and quran_x_time != None
                    and quran_yvalue != None
                ):
                    if debug:
                        print(
                            "append quran_x_time=%s quran_yvalue=%s"
                            % (quran_x_time, quran_yvalue)
                        )
                    x.append(quran_x_time)
                    y.append(quran_yvalue)
                # prepare for next round
                quran_x_time = x_time
                quran_yvalue = yvalue

            oldSeconds = x_time_sec_res
            quran_line = line

        except ValueError:
            print("lines[i]=%s" % (lines[i]))
            print("lines[i].split=%s" % (lines[i].split(" ")))
            sys.exit(1)

    fid.close()
    xnp = np.array(x)
    ynp = np.array(y)

    plt.plot(xnp, ynp)
    plt.gca().xaxis.set_major_formatter(mdates.DateFormatter("%Y-%m-%d %H:%M:%S"))
    if match_ptpstate != None:
        plt.gca().yaxis.set_major_formatter(mticker.FormatStrFormatter("%d PTPstate"))
    else:
        plt.gca().yaxis.set_major_formatter(mticker.FormatStrFormatter("%f usec"))

    for txt in plt.gca().xaxis.get_majorticklabels():
        txt.set_rotation(270)

    plt.tight_layout()
    plt.title(output_file_name)
    if output_file_name is not None:
        if debug:
            print("output_file_name=%s" % (output_file_name))
        plt.savefig(output_file_name)
    plt.show()


if __name__ == "__main__":
    sys.exit(main(sys.argv))
