#!/bin/sh
FILEWITHOUTEXT=monEL6688-YMIR

FALSE=0
export FALSE

rm -f $FILEWITHOUTEXT.*.*
rm -f $FILEWITHOUTEXT*

PATHINHOME=torstenbogershausen@ymir-vm-ioc-01.cn.nin.ess.eu:/home/torstenbogershausen/MCAG_SetupMotionDemo/epics/modules/ethercatmc/test/pythonscripts/$FILEWITHOUTEXT.txt

# Do not mess with the "" and '' below
rsync -av -e 'ssh -o "ProxyJump torstenbogershausen@nxbastion-nin-01.cn.nin.ess.eu"' $PATHINHOME .
if ! test -s $FILEWITHOUTEXT.txt; then
	exit 1
fi

## DCTIME
#PVNAME=DCTIME
#INPUTFILENAME=$FILEWITHOUTEXT.${PVNAME}.txt
#OUTPUTFILENAME=$FILEWITHOUTEXT.${PVNAME}.png
#rm -f $INPUTFILENAME $OUTPUTFILENAME
#if grep "${PVNAME}" $FILEWITHOUTEXT.txt >$INPUTFILENAME; then
#  python unglitchify-and-plot.py $INPUTFILENAME $OUTPUTFILENAME | tee $INPUTFI#LENAME.glitches.txt
#fi

### 1 Hz Pulse, negativ
#PVNAME=UTCEL1252N0
#INPUTFILENAME=$FILEWITHOUTEXT.${PVNAME}.txt
#OUTPUTFILENAME=$FILEWITHOUTEXT.${PVNAME}.png
#rm -f $INPUTFILENAME $OUTPUTFILENAME
#if grep "${PVNAME}" $FILEWITHOUTEXT.txt >$INPUTFILENAME; then
#  python unglitchify-and-plot.py $INPUTFILENAME $OUTPUTFILENAME | tee $INPUTFI#LENAME.glitches.txt
#fi


#PTPOffset
PVNAME=PTPOffset
INPUTFILENAME=$FILEWITHOUTEXT.${PVNAME}.txt
OUTPUTFILENAME=$FILEWITHOUTEXT.${PVNAME}.png
if grep "${PVNAME}" $FILEWITHOUTEXT.txt >$INPUTFILENAME; then
  python unglitchify-and-plot.py $INPUTFILENAME $OUTPUTFILENAME
fi


#PTPState
PVNAME=PTPState
INPUTFILENAME=$FILEWITHOUTEXT.${PVNAME}.txt
OUTPUTFILENAME=$FILEWITHOUTEXT.${PVNAME}.png
if grep "${PVNAME}.*[0-9]" $FILEWITHOUTEXT.txt >$INPUTFILENAME; then
  python unglitchify-and-plot.py $INPUTFILENAME $OUTPUTFILENAME
fi

#DcToExtTimeOffsetSystem
#PVNAME=DcToExtTimeOffsetSystem
#INPUTFILENAME=$FILEWITHOUTEXT.${PVNAME}.txt
#OUTPUTFILENAME=$FILEWITHOUTEXT.${PVNAME}.png
#if grep "${PVNAME}" $FILEWITHOUTEXT.txt >$INPUTFILENAME; then
#  python unglitchify-and-plot.py $INPUTFILENAME $OUTPUTFILENAME
#fi

if test $? -eq 0; then
  ls -l $FILEWITHOUTEXT*
fi
