#!/bin/sh
FILEWITHOUTEXT=EncAct

FALSE=0
export FALSE

#PATHINHOME=MCAG_setupMotionDemo.191003-base-7.0.5/epics/modules/ethercatmc/test/pythonscripts
PATHINHOME=MCAG_setupMotionDemo/epics/modules/ethercatmc/test/pythonscripts
if ! test -s $FILEWITHOUTEXT.txt; then
  IPADDR=172.30.242.52
  #IOC for Crate 007
  #IPADDR=172.30.244.38
  if ping -c 1 $IPADDR; then
    rsync -v -v torstenbogershausen@$IPADDR:/home/torstenbogershausen/$PATHINHOME/$FILEWITHOUTEXT.txt ./$FILEWITHOUTEXT.txt
  fi
fi


## 1 Hz Pulse, via PILS, new format
PVNAME=UTCEL1252P0
INPUTFILENAME=$FILEWITHOUTEXT.${PVNAME}.txt
OUTPUTFILENAME=$FILEWITHOUTEXT.${PVNAME}.png
rm -f $INPUTFILENAME $OUTPUTFILENAME
if grep "${PVNAME}" $FILEWITHOUTEXT.txt >$INPUTFILENAME; then
  python unglitchify-and-plot.py $INPUTFILENAME $OUTPUTFILENAME | tee $INPUTFI#LENAME.glitches.txt
fi

## 1 Hz Pulse, negativ
PVNAME=UTCEL1252N0
INPUTFILENAME=$FILEWITHOUTEXT.${PVNAME}.txt
OUTPUTFILENAME=$FILEWITHOUTEXT.${PVNAME}.png
rm -f $INPUTFILENAME $OUTPUTFILENAME
if grep "${PVNAME}" $FILEWITHOUTEXT.txt >$INPUTFILENAME; then
  python unglitchify-and-plot.py $INPUTFILENAME $OUTPUTFILENAME | tee $INPUTFI#LENAME.glitches.txt
fi


#PTPOffset
PVNAME=PTPOffset
INPUTFILENAME=$FILEWITHOUTEXT.${PVNAME}.txt
OUTPUTFILENAME=$FILEWITHOUTEXT.${PVNAME}.png
if grep "${PVNAME}" $FILEWITHOUTEXT.txt >$INPUTFILENAME; then
  python unglitchify-and-plot.py $INPUTFILENAME $OUTPUTFILENAME
fi


#PTPState
PVNAME=PTPState
INPUTFILENAME=$FILEWITHOUTEXT.${PVNAME}.txt
OUTPUTFILENAME=$FILEWITHOUTEXT.${PVNAME}.png
if grep "${PVNAME}.*[0-9]" $FILEWITHOUTEXT.txt >$INPUTFILENAME; then
  python unglitchify-and-plot.py $INPUTFILENAME $OUTPUTFILENAME
fi

#DcToExtTimeOffsetSystem
#PVNAME=DcToExtTimeOffsetSystem
#INPUTFILENAME=$FILEWITHOUTEXT.${PVNAME}.txt
#OUTPUTFILENAME=$FILEWITHOUTEXT.${PVNAME}.png
#if grep "${PVNAME}" $FILEWITHOUTEXT.txt >$INPUTFILENAME; then
#  python unglitchify-and-plot.py $INPUTFILENAME $OUTPUTFILENAME
#fi

if test $? -eq 0; then
  ls -l $FILEWITHOUTEXT*
fi
